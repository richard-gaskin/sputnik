# Sputnik Site

## Download
```
git clone https://bitbucket.org/richard-gaskin/sputnik.git
```

## Project setup
```
npm install
```

### Run Comand Below to run a local server of the Project.
```
npm run serve
```
